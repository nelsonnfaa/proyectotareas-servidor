const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
  //leer token
  const token = req.header('x-auth-token');
  //console.log(token);
  //revisar token
  if (!token) {
    return res.status(401).json({ msg: 'No hay token' });
  }
  //validar token
  try {
    const cifrado = jwt.verify(token, process.env.SECRET_KEY);
    req.usuario = cifrado.usuario;
    next();
  } catch (error) {
    return res.status(401).json({ msg: 'Token no valido' });
  }
};
