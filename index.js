const express = require('express');
const app = express();
const conectarDB = require('./config/db');
const cors = require('cors');
app.use(express.json({ extended: true }));
//conectamos a la bbdd
conectarDB();

//habilitar cors
app.use(cors());

//puerto de la app
const port = process.env.PORT || 4000;

//importar rutas
app.use('/api/usuarios', require('./routes/usuarios'));
app.use('/api/auth', require('./routes/auth'));
app.use('/api/proyectos', require('./routes/proyectos'));
app.use('/api/tareas', require('./routes/tareas'));

app.listen(port, '0.0.0.0', () =>
  console.log(`listening on http://localhost:${port}`)
);
