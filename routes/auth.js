//ruta para autenticar usuarios
const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');

// crea un usuario

router.post(
  '/',
  /*[
    check('email', 'Agregar un email válido').isEmail(),
    check('password', 'El password debe ser minimo de 6 caracteres').isLength({
      min: 6,
    }),
  ],*/
  authController.autenticarUsuario
);

//autenticar
router.get('/', auth, authController.usuarioAutenticado);

module.exports = router;
