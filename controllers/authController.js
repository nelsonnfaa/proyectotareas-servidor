const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.autenticarUsuario = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    const erro = errores.array()
    // console.log(erro[0].msg);
    
    return res.status(400).json({ msg: erro[0].msg });
  }
  const { email, password } = req.body;
  try {
    //revisar que sea usuario registrado
    let usuario = await Usuario.findOne({ email });
    if (!usuario) {
      return res.status(400).json({ msg: 'El usuario no existe' });
    }
    //comprobamos el password
    const passCorrect = await bcryptjs.compare(password, usuario.password);
    if (!passCorrect) {
      return res.status(400).json({ msg: 'La contraseña es incorrecta' });
    }
    //crear y firmar el jwt
    const payload = {
      usuario: {
        id: usuario.id,
      },
    };
    //firmar jwt
    jwt.sign(
      payload,
      process.env.SECRET_KEY,
      {
        expiresIn: 3600,
      },
      (error, token) => {
        if (error) throw error;
        //respuesta
        res.status(200).json({ token });
      }
    );
  } catch (error) {
    console.log(error);
  }
};

//Obtine usuario autenticado

exports.usuarioAutenticado = async (req, res) => {
  try {
    const usuario = await Usuario.findById(req.usuario.id).select('-password');
    return res.json(usuario);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: 'Hubo un error' });
  }
};
