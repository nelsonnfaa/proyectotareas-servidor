const Proyecto = require('../models/Proyecto');
const { validationResult } = require('express-validator');

const crearProyecto = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  try {
    //crear nuevo proyecto
    const proyecto = new Proyecto(req.body);
    //guardar el creador
    proyecto.creador = req.usuario.id;
    await proyecto.save();
    res.json(proyecto);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: 'Error al crear proyecto' });
  }
};

//obtener los proyectos del usuario
const obtenerProyectos = async (req, res) => {
  try {
    //console.log(req.usuario);
    const proyectos = await Proyecto.find({ creador: req.usuario.id });
    res.json(proyectos);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: 'Error al obtener proyectos' });
  }
};

//actualiza un proyecto
const actualizarProyecto = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  //extraer info proyecto
  const { nombre } = req.body;
  const nuevoProyecto = {};
  if (nombre) {
    nuevoProyecto.nombre = nombre;
  }

  try {
    //revisar id
    let proyectofound = await Proyecto.findById(req.params.id);
    //Si el proyecto existe o no
    if (!proyectofound) {
      return res.status(404).json({
        msg: 'Proyecto no encontrado',
      });
    }

    //debe ser el mismo usuario que modifica y crea
    if (proyectofound.creador.toString() !== req.usuario.id) {
      return res.status(401).json({
        msg: 'No autorizado',
      });
    }

    //actualizar
    let proyecto = await Proyecto.findByIdAndUpdate(
      {
        _id: req.params.id,
      },
      { $set: nuevoProyecto },
      { new: true }
    );
    res.json({ proyecto });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: 'Error al actualizar proyecto' });
  }
};

//elimiar por id
const eliminarProyecto = async (req, res) => {
  try {
    //revisar id
    //console.log(req.params);
    let proyecto = await Proyecto.findById(req.params.id);
    //Si el proyecto existe o no
    if (!proyecto) {
      return res.status(404).json({
        msg: 'Proyecto no encontrado',
      });
    }

    //debe ser el mismo usuario que modifica y crea
    if (proyecto.creador.toString() !== req.usuario.id) {
      return res.status(401).json({
        msg: 'No autorizado',
      });
    }
    await Proyecto.findByIdAndDelete(req.params.id);
    return res.json({ msg: 'Proyecto eliminado.' });
  } catch (error) {
    //console.log(error);
    return res.status(500).json({ msg: 'Error al eliminar el proyecto' });
  }
};

module.exports = {
  crearProyecto,
  obtenerProyectos,
  actualizarProyecto,
  eliminarProyecto,
};
