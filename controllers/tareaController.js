const Tarea = require('../models/Tarea');
const Proyecto = require('../models/Proyecto');
const { validationResult } = require('express-validator');

//crea una nueva tarea
const crearTarea = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  const { proyecto } = req.body;
  try {
    const proyectoFound = await Proyecto.findById(proyecto);
    if (!proyectoFound) {
      return res.status(404).json({ msg: 'Proyecto no encontrado' });
    }
    //el proyecto pertene al usuario logado
    if (proyectoFound.creador.toString() !== req.usuario.id) {
      return res.status(401).json({
        msg: 'No autorizado',
      });
    }
    //creamos tarea
    const tarea = new Tarea(req.body);
    tarea.save();
    return res.status(200).json(tarea);
  } catch (error) {
    console.log(error);
    res.status(500).send('Hubo un error');
  }
};

//obtener todas las tareas dado id de proyecto
const obtenerTareas = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  const { proyecto } = req.query;
  //console.log('sdsad::::::::::::', proyecto);

  try {
    const proyectoFound = await Proyecto.findById(proyecto);
    if (!proyectoFound) {
      return res.status(404).json({ msg: 'Proyecto no encontrado' });
    }
    //el proyecto pertene al usuario logado
    if (proyectoFound.creador.toString() !== req.usuario.id) {
      return res.status(401).json({
        msg: 'No autorizado',
      });
    }
    //recuperamos tareas de un proyecto
    const tareas = await Tarea.find({ proyecto });
    return res.status(200).json(tareas);
  } catch (error) {
    console.log(error);
    res.status(500).send('Hubo un error');
  }
};

//actualizar tarea
const actualizarTarea = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }

  const { proyecto, nombre, estado } = req.body.params.tarea;
  const { id } = req.params;
  try {
    const proyectoFound = await Proyecto.findById(proyecto);
    const tareaFound = await Tarea.findById(id);
    if (!proyectoFound || !tareaFound) {
      return res.status(404).json({ msg: 'Proyecto o tarea no encontrados' });
    }
    //el proyecto pertene al usuario logado
    if (proyectoFound.creador.toString() !== req.usuario.id) {
      return res.status(401).json({
        msg: 'No autorizado',
      });
    }
    //actualizamos tarea
    const updatedTarea = {
      nombre: nombre || tareaFound.nombre,
      estado: estado || tareaFound.estado,
    };

    tareaactu = await Tarea.findByIdAndUpdate(id, updatedTarea, { new: true });
    return res.status(200).json({ tareaactu });
  } catch (error) {
    console.log(error);
    res.status(500).send('Hubo un error');
  }
};

const eliminarTarea = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }
  const { proyecto } = req.query;
  const { id } = req.params;

  try {
    const proyectoFound = await Proyecto.findById(proyecto);
    const tareaFound = await Tarea.findById(id);
    if (!proyectoFound || !tareaFound) {
      return res.status(404).json({ msg: 'Proyecto o tarea no encontrados' });
    }
    //el proyecto pertene al usuario logado
    if (proyectoFound.creador.toString() !== req.usuario.id) {
      return res.status(401).json({
        msg: 'No autorizado',
      });
    }
    //eliminamos tarea
    const tarea = await Tarea.findByIdAndDelete(id);
    return res.status(200).json({ msg: 'tarea eliminada' });
  } catch (error) {
    console.log(error);
    res.status(500).send('Hubo un error');
  }
};

module.exports = {
  crearTarea,
  obtenerTareas,
  actualizarTarea,
  eliminarTarea,
};
